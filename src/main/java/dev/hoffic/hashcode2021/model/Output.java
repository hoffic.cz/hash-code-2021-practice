package dev.hoffic.hashcode2021.model;

import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;

public class Output {
    public List<Delivery> twoTeamPizzas;
    public List<Delivery> threeTeamPizzas;
    public List<Delivery> fourTeamPizzas;

    public Output() {
        this.twoTeamPizzas = new LinkedList<>();
        this.threeTeamPizzas = new LinkedList<>();
        this.fourTeamPizzas = new LinkedList<>();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Output.class.getSimpleName() + "[", "]")
                .add("twoTeamPizzas=" + twoTeamPizzas)
                .add("threeTeamPizzas=" + threeTeamPizzas)
                .add("fourTeamPizzas=" + fourTeamPizzas)
                .toString();
    }
}
