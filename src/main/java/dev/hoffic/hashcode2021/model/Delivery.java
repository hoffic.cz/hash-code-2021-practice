package dev.hoffic.hashcode2021.model;

import java.util.LinkedList;
import java.util.List;

public class Delivery {
    public List<Pizza> pizzas;

    public Delivery() {
        this.pizzas = new LinkedList<>();
    }

    public int calculateScore() {
        var uniqueIngredients = (int) pizzas.stream()
                .flatMap(pizza -> pizza.ingredients.stream())
                .distinct()
                .count();
        return uniqueIngredients * uniqueIngredients;
    }
}
