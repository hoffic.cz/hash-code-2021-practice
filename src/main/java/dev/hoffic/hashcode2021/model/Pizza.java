package dev.hoffic.hashcode2021.model;

import java.util.Set;

public class Pizza {

    public Pizza(int id, Set<Integer> ingredients) {
        this.id = id;
        this.ingredients = ingredients;
    }
    
    public int id;
    public Set<Integer> ingredients;

    @Override
    public boolean equals(Object pizza) {
        return id == ((Pizza) pizza).id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
