package dev.hoffic.hashcode2021.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringJoiner;

public class Input {

    public int pizzasAvailable = Integer.MIN_VALUE;
    public int twoPersonTeams = Integer.MIN_VALUE;
    public int threePersonTeams = Integer.MIN_VALUE;
    public int fourPersonTeams = Integer.MIN_VALUE;

    public ArrayList<Pizza> pizzas;
    public HashMap<String, Integer> ingredientsMap;

    public Input() {
        this.pizzas = new ArrayList<>();
        this.ingredientsMap = new HashMap<>();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Input.class.getSimpleName() + "[", "]")
                .add("pizzasAvailable=" + pizzasAvailable)
                .add("twoPersonTeams=" + twoPersonTeams)
                .add("threePersonTeams=" + threePersonTeams)
                .add("fourPersonTeams=" + fourPersonTeams)
                .add("pizzas=" + pizzas)
                .toString();
    }
}
