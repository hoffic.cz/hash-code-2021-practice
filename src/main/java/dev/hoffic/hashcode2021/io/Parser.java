package dev.hoffic.hashcode2021.io;

import dev.hoffic.hashcode2021.model.Input;
import dev.hoffic.hashcode2021.model.Pizza;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;

public class Parser {

    final static String PATH = "input/";

    public Input parseFile(String problem) throws FileNotFoundException {
        System.out.println("Parsing problem " + problem);

        var input = new Input();

        try (var scanner = new Scanner(new File(PATH + problem + ".in"))) {
            var header = scanner.nextLine();
            try (var headerScanner = new Scanner(header)) {
                input.pizzasAvailable = headerScanner.nextInt();
                input.twoPersonTeams = headerScanner.nextInt();
                input.threePersonTeams = headerScanner.nextInt();
                input.fourPersonTeams = headerScanner.nextInt();
            }

            var ingredientsCount = 0;

            var pizzaId = 0;
            while (scanner.hasNextLine()) {
                var pizza = scanner.nextLine();
                try (var pizzaScanner = new Scanner(pizza)) {
                    pizzaScanner.nextInt();

                    var ingredients = new HashSet<Integer>();
                    while (pizzaScanner.hasNext()) {
                        var ingredientText = pizzaScanner.next();
                        if (!input.ingredientsMap.containsKey(ingredientText)) {
                            input.ingredientsMap.put(ingredientText, ingredientsCount++);
                        }

                        ingredients.add(input.ingredientsMap.get(ingredientText));
                    }

                    input.pizzas.add(new Pizza(pizzaId, ingredients));
                    pizzaId++;
                }
            }
        }

        return input;
    }
}
