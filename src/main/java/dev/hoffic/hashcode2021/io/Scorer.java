package dev.hoffic.hashcode2021.io;

import dev.hoffic.hashcode2021.model.Delivery;
import dev.hoffic.hashcode2021.model.Output;

import java.util.Collection;
import java.util.stream.Stream;

public class Scorer {
    public int calculateScore(Output solution) {
        return Stream.of(solution.fourTeamPizzas, solution.threeTeamPizzas, solution.twoTeamPizzas)
                .flatMap(Collection::stream)
                .mapToInt(Delivery::calculateScore)
                .sum();
    }
}
