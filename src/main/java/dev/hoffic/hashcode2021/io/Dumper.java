package dev.hoffic.hashcode2021.io;

import dev.hoffic.hashcode2021.model.Output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

public class Dumper {
    final static String PATH = "output/";

    public void dump(Output output, String problem) throws FileNotFoundException {
        var writer = new PrintWriter(PATH + problem + ".out");

        var deliveries = output.twoTeamPizzas.size()
                + output.threeTeamPizzas.size()
                + output.fourTeamPizzas.size();

        writer.println(deliveries);

        for (var delivery : output.twoTeamPizzas) {
            var pizzaLine = delivery.pizzas.stream()
                    .map(pizza -> String.valueOf(pizza.id))
                    .collect(Collectors.joining(" "));
            writer.println("2 " + pizzaLine);
        }
        for (var delivery : output.threeTeamPizzas) {
            var pizzaLine = delivery.pizzas.stream()
                    .map(pizza -> String.valueOf(pizza.id))
                    .collect(Collectors.joining(" "));
            writer.println("3 " + pizzaLine);
        }
        for (var delivery : output.fourTeamPizzas) {
            var pizzaLine = delivery.pizzas.stream()
                    .map(pizza -> String.valueOf(pizza.id))
                    .collect(Collectors.joining(" "));
            writer.println("4 " + pizzaLine);
        }

        writer.flush();
    }
}
