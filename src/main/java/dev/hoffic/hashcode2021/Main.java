package dev.hoffic.hashcode2021;

import dev.hoffic.hashcode2021.io.Dumper;
import dev.hoffic.hashcode2021.io.Parser;
import dev.hoffic.hashcode2021.io.Scorer;
import dev.hoffic.hashcode2021.logic.Computer;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Hello Bára!");
        var problemName = args[0];

        System.out.println("Parsing...");
        var input = new Parser().parseFile(problemName);
        System.out.println(input.toString());

        System.out.println("Computing...");
        var output = new Computer().compute(input);
        System.out.println(output);

        System.out.println("Dumping...");
        new Dumper().dump(output, problemName);

        System.out.printf("Score: %d", new Scorer().calculateScore(output));
    }
}
