package dev.hoffic.hashcode2021.util;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.StringJoiner;

public class Watch {
    private final long RESOLUTION = 100;

    private long goal;
    private long step = 1;
    private long last;

    public Watch(int remaining) {
        goal = remaining;
        last = System.nanoTime();
    }

    public void tick(Object... props) {
        if (step % RESOLUTION == 0) {
            var joiner = new StringJoiner(", ");

            Arrays.stream(props).forEach(o -> joiner.add(String.valueOf(o)));

            var lapTime = 1.0 * (System.nanoTime() - last) / 1_000_000_000;
            if (lapTime == 0) {
                lapTime = -1;
            }
            last = System.nanoTime();

            var speed = RESOLUTION / lapTime;
            joiner.add(String.format("Speed: %.2f iter./s", speed));

            var left = (goal - step) / speed;
            joiner.add(String.format("Left: %.1f min", left / 60));

            var eta = LocalDateTime.now().plusSeconds(Math.round(left));
            joiner.add(String.format("ETA: %s", eta.format(DateTimeFormatter.ISO_LOCAL_TIME)));

            System.out.println(joiner.toString());
        }
        step++;
    }
}
