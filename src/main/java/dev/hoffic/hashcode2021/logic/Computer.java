package dev.hoffic.hashcode2021.logic;

import dev.hoffic.hashcode2021.model.Delivery;
import dev.hoffic.hashcode2021.model.Input;
import dev.hoffic.hashcode2021.model.Output;
import dev.hoffic.hashcode2021.model.Pizza;
import dev.hoffic.hashcode2021.util.Watch;

import java.util.*;

public class Computer {
    final static int SEARCH_LIMIT = Integer.MAX_VALUE;

    public Output compute(Input input) {
        var output = new Output();

        input.pizzas.sort(Comparator.comparingInt(pizza -> -pizza.ingredients.size()));
        var remainingPizzas = new LinkedHashSet<>(input.pizzas);
        var remainingTwoTeams = input.twoPersonTeams;
        var remainingThreeTeams = input.threePersonTeams;
        var remainingFourTeams = input.fourPersonTeams;

        var watch = new Watch(remainingPizzas.size());

        while (remainingPizzas.size() >= 4 && remainingFourTeams > 0) {
            var delivery = getDelivery(4, remainingPizzas);

            output.fourTeamPizzas.add(delivery);
            remainingFourTeams--;

            watch.tick(remainingFourTeams, remainingPizzas.size());
        }

        while (remainingPizzas.size() >= 3 && remainingThreeTeams > 0) {
            var delivery = getDelivery(3, remainingPizzas);

            output.threeTeamPizzas.add(delivery);
            remainingThreeTeams--;

            watch.tick(remainingThreeTeams, remainingPizzas.size());
        }

        while (remainingPizzas.size() >= 2 && remainingTwoTeams > 0) {
            var delivery = getDelivery(2, remainingPizzas);

            output.twoTeamPizzas.add(delivery);
            remainingTwoTeams--;

            watch.tick(remainingTwoTeams, remainingPizzas.size());
        }

        // Iterate over the pizzas
        // Choose the best available
        // Find the best complementing
        // Decide if worth complimenting into 3-team
        // Decide if worth complimenting into 4-team

        return output;
    }

    public Delivery getDelivery(int numberOfPizzas, LinkedHashSet<Pizza> remainingPizzas) {
        var delivery = new Delivery();
        var includedIngredients = new HashSet<Integer>();

        // FIRST PIZZA
        var firstPizza = remainingPizzas.stream().findFirst().get();
        remainingPizzas.remove(firstPizza);
        includedIngredients.addAll(firstPizza.ingredients);
        delivery.pizzas.add(firstPizza);

        for (int i = 0; i < numberOfPizzas - 1; i++) {
            var otherPizza = findBestComplimentingPizza(includedIngredients, remainingPizzas);
            remainingPizzas.remove(otherPizza);
            includedIngredients.addAll(otherPizza.ingredients);
            delivery.pizzas.add(otherPizza);
        }

        return delivery;
    }

    public Pizza findBestComplimentingPizza(HashSet<Integer> ingredients, LinkedHashSet<Pizza> remainingPizzas) {
        var pizzaScores = new HashMap<Pizza, Double>();
        remainingPizzas.forEach(pizza -> pizzaScores.put(
                pizza,
                calculateFitness(ingredients, pizza)));

        // TODO: Implement limit
        return remainingPizzas.stream().max(Comparator.comparing(pizzaScores::get)).get();
    }

    public double calculateFitness(HashSet<Integer> existingIngredients, Pizza other) {
        var deliveryIngredientsCount = existingIngredients.size(); // A
        var otherIngredientsCount = other.ingredients.size(); // B
        var extraIngredients = calculateNumberOfExtraIngredients(existingIngredients, other); // C

        var score = Math.pow(deliveryIngredientsCount + extraIngredients, 2); // (A + C) ^ 2
        var effectiveness = 1.0 * otherIngredientsCount / extraIngredients; // C / B

//        return score * effectiveness;
        return extraIngredients - (other.ingredients.size() - extraIngredients);
    }

    /**
     * Returns number of extra ingredients in Pizza b, compare to the delivery;
     */
    public int calculateNumberOfExtraIngredients(HashSet<Integer> deliveryIngredients, Pizza b) {
        return (int) b.ingredients.stream()
                .filter(integer -> !deliveryIngredients.contains(integer))
                .count();
    }
}
